-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema control_asesoria_db
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema control_asesoria_db
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `control_asesoria_db` DEFAULT CHARACTER SET utf8 ;
USE `control_asesoria_db` ;

-- -----------------------------------------------------
-- Table `control_asesoria_db`.`persona`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `control_asesoria_db`.`persona` (
  `idPersona` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(75) NULL,
  `ap_paterno` VARCHAR(45) NULL,
  `ap_materno` VARCHAR(45) NULL,
  `email` VARCHAR(100) NULL,
  `matricula` CHAR(8) NULL,
  PRIMARY KEY (`idPersona`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `control_asesoria_db`.`profesor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `control_asesoria_db`.`profesor` (
  `idProfesor` INT NOT NULL AUTO_INCREMENT,
  `idPersona` INT NULL,
  `status` CHAR(1) NULL,
  `password` VARCHAR(100) NULL,
  PRIMARY KEY (`idProfesor`),
  INDEX `PersonaProfesor_idx` (`idPersona` ASC) VISIBLE,
  CONSTRAINT `PersonaProfesor`
    FOREIGN KEY (`idPersona`)
    REFERENCES `control_asesoria_db`.`persona` (`idPersona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `control_asesoria_db`.`periodo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `control_asesoria_db`.`periodo` (
  `idperiodo` INT NOT NULL AUTO_INCREMENT,
  `serie` VARCHAR(100) NULL,
  `numero_periodo` INT NULL,
  `asistencia` CHAR(1) NULL,
  PRIMARY KEY (`idperiodo`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `control_asesoria_db`.`proyecto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `control_asesoria_db`.`proyecto` (
  `idProyecto` INT NOT NULL AUTO_INCREMENT,
  `idPeriodo` INT NULL,
  `nombre` VARCHAR(50) NULL,
  `descripcion` VARCHAR(45) NULL,
  `status` CHAR(1) NULL,
  `fecha_registro` DATETIME NULL,
  PRIMARY KEY (`idProyecto`),
  INDEX `PeriodoProyecto_idx` (`idPeriodo` ASC) VISIBLE,
  CONSTRAINT `PeriodoProyecto`
    FOREIGN KEY (`idPeriodo`)
    REFERENCES `control_asesoria_db`.`periodo` (`idperiodo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `control_asesoria_db`.`alumno`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `control_asesoria_db`.`alumno` (
  `idAlumno` INT NOT NULL AUTO_INCREMENT,
  `idPersona` INT NULL,
  `idProyecto` INT NULL,
  `idProfesor` INT NULL,
  PRIMARY KEY (`idAlumno`),
  INDEX `AlumnoPersona_idx` (`idPersona` ASC) VISIBLE,
  INDEX `AlumnoProfesor_idx` (`idProfesor` ASC) VISIBLE,
  INDEX `AlumnoProyecto_idx` (`idProyecto` ASC) VISIBLE,
  CONSTRAINT `AlumnoPersona`
    FOREIGN KEY (`idPersona`)
    REFERENCES `control_asesoria_db`.`persona` (`idPersona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `AlumnoProfesor`
    FOREIGN KEY (`idProfesor`)
    REFERENCES `control_asesoria_db`.`profesor` (`idProfesor`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `AlumnoProyecto`
    FOREIGN KEY (`idProyecto`)
    REFERENCES `control_asesoria_db`.`proyecto` (`idProyecto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `control_asesoria_db`.`empresa`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `control_asesoria_db`.`empresa` (
  `idEmpresa` INT NOT NULL AUTO_INCREMENT,
  `razon_social` VARCHAR(85) NULL,
  `direccion` TEXT NULL,
  `status` CHAR(1) NULL,
  PRIMARY KEY (`idEmpresa`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `control_asesoria_db`.`alumno_empresa`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `control_asesoria_db`.`alumno_empresa` (
  `idAlumno_empresa` INT NOT NULL AUTO_INCREMENT,
  `idAlumno` INT NULL,
  `idEmpresa` INT NULL,
  PRIMARY KEY (`idAlumno_empresa`),
  INDEX `AlumnoE_idx` (`idEmpresa` ASC) VISIBLE,
  INDEX `EmpresaA_idx` (`idAlumno` ASC) VISIBLE,
  CONSTRAINT `AlumnoE`
    FOREIGN KEY (`idEmpresa`)
    REFERENCES `control_asesoria_db`.`empresa` (`idEmpresa`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `EmpresaA`
    FOREIGN KEY (`idAlumno`)
    REFERENCES `control_asesoria_db`.`alumno` (`idAlumno`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `control_asesoria_db`.`actividades`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `control_asesoria_db`.`actividades` (
  `idActividades` INT NOT NULL AUTO_INCREMENT,
  `idPeriodo` INT NULL,
  `descripcion` TEXT NULL,
  `ponderacion` INT NULL,
  PRIMARY KEY (`idActividades`),
  INDEX `Periodo_idx` (`idPeriodo` ASC) VISIBLE,
  CONSTRAINT `Periodo`
    FOREIGN KEY (`idPeriodo`)
    REFERENCES `control_asesoria_db`.`periodo` (`idperiodo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `control_asesoria_db`.`tipo_contacto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `control_asesoria_db`.`tipo_contacto` (
  `idTipo_contacto` INT NOT NULL AUTO_INCREMENT,
  `tipo` VARCHAR(100) NULL,
  `dato` VARCHAR(100) NULL,
  PRIMARY KEY (`idTipo_contacto`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `control_asesoria_db`.`contacto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `control_asesoria_db`.`contacto` (
  `idContacto` INT NOT NULL AUTO_INCREMENT,
  `nombre_contacto` VARCHAR(100) NULL,
  `idEmpresa` INT NULL,
  `idTipoContacto` INT NULL,
  PRIMARY KEY (`idContacto`),
  INDEX `EmpresaContacto_idx` (`idEmpresa` ASC) VISIBLE,
  INDEX `ContactoTipoContacto_idx` (`idTipoContacto` ASC) VISIBLE,
  CONSTRAINT `EmpresaContacto`
    FOREIGN KEY (`idEmpresa`)
    REFERENCES `control_asesoria_db`.`empresa` (`idEmpresa`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `ContactoTipoContacto`
    FOREIGN KEY (`idTipoContacto`)
    REFERENCES `control_asesoria_db`.`tipo_contacto` (`idTipo_contacto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
