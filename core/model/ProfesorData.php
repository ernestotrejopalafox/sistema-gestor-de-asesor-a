<?php
    class ProfesorData
    {    
        public static $table_name = "profesor";

        public function __construct()
        {
            $this->idProfesor = "";
            $this->idPersona = "";
            $this->status = 1;    
            $this->password  = "";    
        }        

        public function add()
        {
            $query = insertsql("INSERT INTO ".self::$table_name." SET idPersona = '$this->idPersona', status = '$this->status' , password = '$this->password'");
            return $query;//regresa el id registrado
        }

        public static function getByMatricula($matricula)
        {
            $query = "SELECT P.*, Pr.* FROM ".self::$table_name." as Pr INNER JOIN persona as P on (P.idPersona = Pr.idPersona)
            WHERE P.matricula = '$matricula' AND Pr.status = '1' LIMIT 1;";
            return Model::one($query, new ProfesorData());//ONE OBTIENE SOLO UN REGISTRO
        }

        public static function getById($idProfesor)
        {
            $query = "SELECT P.* FROM ".self::$table_name." as P WHERE P.idProfesor = '$idProfesor' AND P.status = '1' LIMIT 1;";
            return Model::one($query, new ProfesorData());//ONE OBTIENE SOLO UN REGISTRO
        }

        public static function getAll()
        {
            $query = "SELECT * FROM ".self::$table_name." WHERE status = '1'";
            return Model::many($query, new ProfesorData());
        }        
    }
?>