<?php
	
	require("ConfiguracionDatabase.php");
	require("Sql.php");
    require("SesionSegura.php");
    require("ValidarSesion.php");
    require("Utilities.php");
    require("Diseno.php");
    require("Model.php");


	$ip = IP;
	$fecha = FECHA;
	
	if(SSL)
	{
        if ( !isset($_SERVER['HTTPS']) || strtolower($_SERVER['HTTPS']) != 'on' ) {
        header("Location: " . "https://" . $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"]);
        }    
    }
?>