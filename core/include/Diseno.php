<?php
    class Structure {
		function sidebarnav()
		{          
            $sidebarnav = '
                    <ul id="sidebarnav" class="p-t-30">
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="index.html" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Dashboard</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="fa fa-user"></i><span class="hide-menu">Profesor </span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item"><a href="index.php?view=newProfesor" class="sidebar-link"><i class="fa fa-plus"></i><span class="hide-menu"> Nuevo </span></a></li>

                                <li class="sidebar-item"><a href="index.php?view=profesor" class="sidebar-link"><i class="mdi mdi-calendar-text"></i><span class="hide-menu"> Consulta </span></a></li>
                            </ul>                            
                        </li>
                    </ul>
            ';
			echo $sidebarnav;
        }

        function footer()
        {
            $footer = '
            <footer class="footer text-center">
                Todos los derechos reservados <a href="#">Sistemas TI @2020</a>.
            </footer>
            ';
            echo $footer;
        }
	}
?>


