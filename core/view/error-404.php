<div class="row">
    <div class="col-md-12">
        <div class="card ">
                <div class="error-body text-center">
                    <h1 class="error-title text-danger">404</h1>
                    <h3 class="text-uppercase error-subtitle">PÁGINA NO ENCONTRADA !</h3>
                    <p class="text-muted m-t-30 m-b-30">PUEDES INTENTAR REGRESANDO AL INICIO</p>
                    <a href="index.php?view=dashboard" class="btn btn-danger btn-rounded waves-effect waves-light m-b-40">IR AL INICIO</a>
                </div>
        </div>
    </div>
</div>