

<div class="card">
    <div class="card-body">
        <h5 class="card-title">Consulta de profesores</h5>
        <hr>
        <div class="table-responsive">
            <table id="zero_config" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Matrícula</th>
                        <th>Nombre</th>
                        <th>Email</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $profesores = ProfesorData::getAll();
                        foreach ($profesores as $profesor) 
                        {
                            $persona = PersonaData::getById($profesor->idPersona);
                            ?>
                                <tr>
                                    <td><?=$persona->matricula?></td>
                                    <td><?=$persona->nombre." ".$persona->ap_paterno." ".$persona->ap_materno?></td>
                                    <td><?=$persona->email?></td>
                                    <td></td>
                                </tr>
                            <?php
                        }
                    ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th>Matrícula</th>
                        <th>Nombre</th>
                        <th>Email</th>
                        <th>Opciones</th>
                    </tr>
                </tfoot>
            </table>
        </div>

    </div>
</div>