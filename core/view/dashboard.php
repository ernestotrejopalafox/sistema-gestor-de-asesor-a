<div class="row">
    <!-- Column -->
    <div class="col-md-3 col-lg-3 col-xlg-3">
        <div class="card card-hover">
            <div class="box bg-cyan text-center">
                <h1 class="font-light text-white"><i class="mdi mdi-calendar"></i></h1>
                <h6 class="text-white">Citas hoy</h6>
                <h5 class="text-white">25</h5>
            </div>
        </div>
    </div>
    <!-- Column -->
    <div class="col-md-3 col-lg-3 col-xlg-3">
        <div class="card card-hover">
            <div class="box bg-success text-center">
                <h1 class="font-light text-white"><i class="mdi mdi-account-multiple-outline"></i></h1>
                <h6 class="text-white">Pacientes</h6>
                <h5 class="text-white">1200</h5>
            </div>
        </div>
    </div>
    <!-- Column -->
    <div class="col-md-3 col-lg-3 col-xlg-3">
        <div class="card card-hover">
            <div class="box bg-warning text-center">
                <h1 class="font-light text-white"><i class="mdi mdi-account"></i></h1>
                <h6 class="text-white">Doctores</h6>
                <h5 class="text-white">40</h5>
            </div>
        </div>
    </div>
    <!-- Column -->
    <div class="col-md-3 col-lg-3 col-xlg-3">
        <div class="card card-hover">
            <div class="box bg-danger text-center">
                <h1 class="font-light text-white"><i class="mdi mdi-account"></i></h1>
                <h6 class="text-white">Asistentes</h6>
                <h5 class="text-white">12</h5>
            </div>
        </div>
    </div>
</div>