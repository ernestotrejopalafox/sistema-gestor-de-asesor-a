<div class="card ">
    <div class="card-header ">
        <h4 class="card-title">Nuevo registro</h4>
    </div>
    <form action="./index.php?action=addProfesor" name="frmNewProfesor" id="frmNewEmpleado" method="post">
        <div class="card-body ">
            <div class="align-content-center">
                    <div class="row">
                        <div class="col-md-4 ">
                            <div class="form-group">
                                <label>Nombre:</label>
                                <input type="text" class="form-control" required name="nombre" id="nombre" placeholder="Escribe el nombre" value="">
                            </div>
                        </div>
                        <div class="col-md-4 ">
                            <div class="form-group">
                                <label>Apellido Paterno:</label>
                                <input type="text" class="form-control" required name="ap_paterno" id="ap_paterno" placeholder="Escribe el apellido paterno" value="">
                            </div>
                        </div>
                        <div class="col-md-4 ">
                            <div class="form-group">
                                <label>Apellido Materno:</label>
                                <input type="text" class="form-control" required name="ap_materno" id="ap_materno" placeholder="Escribe el apellido materno" value="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 ">
                            <div class="form-group">
                                <label>Email:</label>
                                <input type="email" class="form-control" required name="email" id="email" placeholder="Escribe el email" value="">
                            </div>
                        </div>
                        <div class="col-md-4 ">
                            <div class="form-group">
                                <label>Matrícula:</label>
                                <input maxlength="8" type="text" required name="matricula" id="matricula" class="form-control" placeholder="Escribe tu matrícula">
                            </div>
                        </div>
                    </div>
            </div>
            <hr>
            <div class="align-content-center">
                <button class="btn btn-success btn-lg" type="submit"><i class="fa fa-save"></i> Registrar</button>
            </div>
        </div>
    </form>
</div>