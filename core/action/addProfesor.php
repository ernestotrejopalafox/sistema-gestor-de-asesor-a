<?php

    $persona = new PersonaData();
    $persona->nombre = ssql($_POST['nombre']);
    $persona->ap_paterno = ssql($_POST['ap_paterno']);
    $persona->ap_materno = ssql($_POST['ap_materno']);
    $persona->email = ssql($_POST['email']);
    $persona->matricula = ssql($_POST['matricula']);
    $idPersona = $persona->add();

    $profesor = new ProfesorData();
    $profesor->idPersona = $idPersona;
    $profesor->password = password_hash(ssql($_POST['matricula']), PASSWORD_DEFAULT);
    $profesor->add();

    if ($profesor) 
    {
        //SE IMPRIME UN TOAST CON UN MENSAJE, EL QUE SEA Y EN 5 SEGUNDOS SE MANDA A LA TABLA
        echo '
        <script>
            window.onload = function() {
                toastr.success("Registro éxitoso", "El profesor: '.$_POST['nombre'].' '.$_POST['ap_paterno'].' '.$_POST['ap_materno'].' ");

                setTimeout(function() {
                    window.location = "index.php?view=profesor"; //will redirect to your blog page (an ex: blog.html)
                }, 5000);
            };
        </script>      
        ';
    }
    else
    {
        include("index.php?view=error-500");
    }

?>

<div class="row">
    <div class="col-md-12">
        <div class="card ">
            <div class="text-center">
                <img src="assets/images/loading.gif" class="img-fluid rounded" width="300px" >
                <h3 class="text-uppercase error-subtitle">CARGANDO...</h3>
            </div>            
        </div>
    </div>
</div>
