function addProfesor()
{
    var url = "core/action/addProfesor.php"; // El script a dónde se realizará la petición.
    $.ajax({
        type: "POST",
        url: url,
        data: $("#frmNewProfesor").serialize(), // Adjuntar los campos del formulario enviado.
        success: function(data) {
            var respuesta = $.parseJSON(data);
            if (respuesta.status === "ok") {
                SuccessMessage(respuesta.mensaje);
                document.getElementById("frmNewProducto").reset();
                setTimeout(function() {
                    window.location = "index.php?s=productos"; //will redirect to your blog page (an ex: blog.html)
                }, 2000);
            } else {
                ErrorMessage(respuesta.mensaje);
                document.getElementById("frmNewProducto").reset();
            }
        }
    });

    return false; // Evitar ejecutar el submit del formulario.
}